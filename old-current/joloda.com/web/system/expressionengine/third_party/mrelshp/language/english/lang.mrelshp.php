<?php

$lang = array(

'mrelshp_license_key' =>
'License Key',


'mrelshp_single' => 
'Single channel',

'mrelshp_optgroups' => 
'Single channel with categories as optgroups',

'mrelshp_multi' => 
'Multi Channels',

'mrelshp_type' =>
'Type',

'mrelshp_rows' =>
'Field rows',

'mrelshp_extended_ui' =>
'jQuery UI Multiselect Widget',


// IGNORE
''=>'');