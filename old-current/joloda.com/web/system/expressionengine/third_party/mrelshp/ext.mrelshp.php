<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
=====================================================
 File: ext.mrelshp.php
-----------------------------------------------------
 Purpose: 	Extands standart relationshipss field as 
 multiple relationships field
-----------------------------------------------------
 Support thread: denis.gorin@gmail.com
=====================================================
 Requires Jquery
=====================================================
 
 Multi Relationships Commercial License: 
 http://devot-ee.com/add-ons/license/multi-relationships/

 Permitted Use
 One license grants the right to perform one installation of the Software.
 Each additional installation of the Software requires an additional purchased license
 
 Buy:
 http://devot-ee.com/add-ons/multi-relationships
 
*/


//error_reporting(E_ALL);
class Mrelshp_ext {

	/** -------------------------------------
	/** Settings
	/** -------------------------------------*/

	var $settings       = array();
	var $name           = 'Multi Relationship Extension';
	var $version        = '1.5.1';
	var $description    = 'Multi Relationship Extension';
	var $settings_exist = 'n';
	var $docs_url       = '';
	
	var $channel_id;
	var $entry_id=0;
	var $_cache = array();
	var $autosave;
	var $field_id;

	/** -------------------------------------
	/** Constructor
	/** -------------------------------------*/
	function Mrelshp_ext($settings='') {
		
		$this->settings = $settings;
	    $this->EE =& get_instance();
		
	}

	/** -------------------------------------
	/** Activate
	/** -------------------------------------*/

	function activate_extension() {
	
 	// Add new extensions
        $ext_template = array(
            'class'    => __CLASS__,
            'settings' => '',
            'priority' => 10,
            'version'  => $this->version,
            'enabled'  => 'y'
        );
        
        $extensions = array(
            array('hook'=>'entry_submission_end', 'method'=>'entry_submission_end'),
            array('hook'=>'entry_submission_ready', 'method'=>'entry_submission_ready'),
            array('hook'=>'channel_entries_tagdata', 'method'=>'channel_entries_tagdata')
        );
        
        foreach($extensions as $extension)
        {
            $ext = array_merge($ext_template, $extension);
            $this->EE->db->insert('exp_extensions', $ext);
        }

	}


	/** -------------------------------------
	/** Update Extension
	/** -------------------------------------*/
	
	function update_extension($current='') 
	{
		if ($current == '' OR $current == $this->version) {
			return FALSE;
		}
	}


	/** -------------------------------------
	/** Disable
	/** -------------------------------------*/
	
	function disable_extension() 
	{
	    $this->EE->db->where('class', 'Mrelshp_ext');
	    $this->EE->db->delete('exp_extensions');
	}

	function prepare($data)
	{
		$this->entry_id = ($data['entry_id']!='')?$data['entry_id']:0;
		$this->channel_id = $data['channel_id'];
		$this->EE->db->select('field_id, field_related_to, field_related_id');
		$query = $this->EE->db->get_where('channel_fields', array('field_type' => 'mrelshp'));
		return $query;
	}
	
	/** -------------------------------------
	/** process
	/** -------------------------------------*/
	function entry_submission_end($entry_id, $meta, $data) 
	{
	
		$query = $this->prepare($data);
		$rel_updates = array();
		$this->entry_id = $entry_id;
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				if (isset($data['field_id_'.$row['field_id']]) && $data['field_id_'.$row['field_id']]!=0)
				{ 
					$this->field_id = $row['field_id'];
					$rel_values = decode_multi_field($data['field_id_'.$row['field_id']]);
					
					foreach ($rel_values as $rel_entry)
					{ 
					
						if (is_numeric($rel_entry))
						{
							$reldata = array(
								'type'			=> $row['field_related_to'],
								'parent_id'		=> $this->entry_id, 
								'child_id'		=> $rel_entry,
								'related_id'	=> $this->channel_id
							);
						
							$rel_updates['field_id_'.$this->field_id][] = $this->EE->functions->compile_relationship($reldata, TRUE);
						
						}
						elseif($rel_entry == '')
						{
							$rel_updates['field_id_'.$this->field_id][] = 0;
						}
					
					}
					
					$this->_cache['rel_updates'] = $rel_updates;
					
				}
			}
		}
	
		if ( /* !$this->autosave && */ isset($this->_cache['rel_updates']) && count($this->_cache['rel_updates']) > 0)
		{
			foreach ($this->_cache['rel_updates'] as $k=>$v)
			{
				$rel_updates = encode_multi_field($v);
				
				$this->EE->db->set($k, $rel_updates);
				$this->EE->db->where('entry_id', $this->entry_id);
				$this->EE->db->update('channel_data');
				$this->EE->db->set('rel_parent_id', $this->entry_id);
				$this->EE->db->where_in('rel_id', $v);
				$this->EE->db->update('relationships');
			}
		}
		return TRUE;

	}
	
	function entry_submission_ready($meta, $data, $autosave) 
	{
		
		/* 
		$this->autosave = $autosave;
		
		if ($this->autosave)
		{
			return;
		}
		*/
		$query = $this->prepare($data);
		
		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row) 
			{
			
				if (isset($data['field_id_'.$row['field_id']]) && count($data['field_id_'.$row['field_id']])>0)
				{ 
					$this->field_id = $row['field_id'];
					
					if ($this->entry_id)
					{
						
						$this->EE->db->select('field_id_'.$row['field_id']);
						$relrow = $this->EE->db->get_where('channel_data', array('entry_id' => $this->entry_id));
						$relrow = $relrow->row_array();
						
						if ($relrow)
						{	
							$relrow = decode_multi_field($relrow['field_id_'.$row['field_id']]);
							
							foreach ($relrow as $rel_entry)
							{
								$this->EE->db->where('rel_id', $rel_entry);
								$this->EE->db->delete('relationships');
							}
						}
					}
				}
			}
		}
		
		return TRUE;
	}
	
	function channel_entries_tagdata($tagdata, $row, $channel)
	{
		$this->EE->load->helper('custom_field');
		$site_id = $channel->EE->config->_global_vars['site_id'];
		foreach($this->EE->TMPL->related_data as $key => $rel_data)
		{
			if(!isset($channel->cfields[$site_id])) continue;
			$field_id = $channel->cfields[$site_id][$rel_data['field_name']];
			preg_match('/'.LD.'related_entries:attributes(.*?)'.RD.'/', $rel_data['tagdata'], $attributes);
			$attributes = (isset($attributes[1]) && count($attributes[1]) > 0)?$this->EE->functions->assign_parameters($attributes[1]):array();
			$this->EE->TMPL->related_data[$key]['tagdata'] = preg_replace('/'.LD.'related_entries:attributes(.*?)'.RD.'/', '', $this->EE->TMPL->related_data[$key]['tagdata']);
			$inner_build = '';
			if ($row['field_id_'.$field_id] != 0) // EE 2.2.2 bugFix
			{	
				$field_data = decode_multi_field($row['field_id_'.$field_id]);
				foreach($field_data as $count => $rel_entry)
				{
					if(isset($attributes['limit']) && $count >= $attributes['limit']) break;
					$channel->rfields[$rel_data['field_name']] = $field_id;
					$channel->related_entries[] = $rel_entry.'_'.$rel_data['marker'];
					$inner_build .= LD.'REL['.$rel_entry.']['.$rel_data['field_name'].']'.$rel_data['marker'].'REL'.RD;
				}
			}	
			$tagdata = str_replace(LD.'REL['.$rel_data['field_name'].']'.$rel_data['marker'].'REL'.RD, $inner_build, $tagdata);
		}
		if (strpos($tagdata, 'mrelshp')!==FALSE)
		{
			if(!isset($channel->cfields[$site_id])) continue;
			preg_match_all('/field=["\']([^"\']+)["\']/',$tagdata, $field_names);
			if (isset($field_names)) $field_names = (count($field_names[1]) > 1)?$field_names[1]:decode_multi_field($field_names[1]);
			foreach ($field_names as $field_name)
			{
				if (isset($field_name)) 
				{
					if (isset($channel->cfields[$site_id][$field_name])) 
					{
						$field_id = $channel->cfields[$site_id][$field_name];
						if (isset($row['field_id_'.$field_id]) && !empty($row['field_id_'.$field_id]))
						{
							$rel_ids_entries = array();
							$rel_ids_query = $this->EE->db->query("SELECT rel_child_id FROM exp_relationships WHERE rel_id IN(". implode(',',decode_multi_field($row['field_id_'.$field_id])) .")");
							if ($rel_ids_query->num_rows() > 0)
							{
								foreach ($rel_ids_query->result_array() as $rel_ids_row)
								{
									$rel_ids_entries = array_merge($rel_ids_entries,decode_multi_field($rel_ids_row['rel_child_id']));
								}
								$rel_ids_entries = encode_multi_field($rel_ids_entries);
								$tagdata = preg_replace('/'.LD.'mrelshp:ids field=["\']'. $field_name .'["\']'.RD.'/', $rel_ids_entries, $tagdata);
							}
						}
						else
						{
							$tagdata = preg_replace('/'.LD.'mrelshp:ids field=["\']'. $field_name .'["\']'.RD.'/', '', $tagdata);//kill tag if no ids
						}
					}
					else
					{
						$tagdata = preg_replace('/'.LD.'mrelshp:ids field=["\']'. $field_name .'["\']'.RD.'/', '', $tagdata);//kill tag if no field
					}
				}
			}
		}
		
		return $tagdata;
	}
	
}