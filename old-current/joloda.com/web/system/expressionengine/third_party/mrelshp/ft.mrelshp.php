<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Multi Relationships Fieldtype
 *
 * @author		Denis Gorin
 * @link		denis.gorin@gmail.com
 
 Multi Relationships Commercial License:
 http://devot-ee.com/add-ons/license/multi-relationships/

Permitted Use
One license grants the right to perform one installation of the Software. 
Each additional installation of the Software requires an additional purchased license.

Buy:
http://devot-ee.com/add-ons/multi-relationships

 */
 
 
 //error_reporting(E_ALL);
 // load class Rel to extend it for multiple relations
// $this->EE =& get_instance();
// $this->EE->api_channel_fields->include_handler('rel');
$_field_type = 'rel';
$_file = 'ft.'.$_field_type.'.php';
$_path = PATH_FT.$_field_type.'/';   
require_once $_path.$_file; 

class Mrelshp_ft extends Rel_ft {

	var $info = array(
		'name'		=> 'Multi Relationships',
		'version'	=> '1.5'
	);
	
	var $view_options = array();
	
	/** -------------------------------------
	/** Constructor
	/** -------------------------------------*/
	function Mrelshp_ft()
	{
		if(APP_VER < '2.1.5') {
			parent::EE_Fieldtype();
		} else {
			parent::__construct();
		} 		
		$this->EE->lang->loadfile('mrelshp');
		$this->EE->load->helper("custom_field_helper");
		$this->view_options = array(
			'single' => lang('mrelshp_single'),
			'optgroups' => lang('mrelshp_optgroups'),
			'multi' => lang('mrelshp_multi')
		);
	}
	
	
	/** -------------------------------------
	/** Theme URL
	/** -------------------------------------*/
	function _asset_path()
	{
		if (! isset($this->cache['theme_url']))
		{
			$theme_folder_url = $this->EE->config->item('theme_folder_url');
			if (substr($theme_folder_url, -1) != '/') $theme_folder_url .= '/';
			$this->cache['theme_url'] = $theme_folder_url.'third_party/mrelshp/assets/';
		}
		return $this->cache['theme_url'];
	}
	
	
	/** -------------------------------------
	/** Display Global Settings
	/** -------------------------------------*/
	
	function display_global_settings()
	{
		$license_key = isset($this->settings['license_key']) ? $this->settings['license_key'] : '';
		$this->EE->load->library('table');
		$this->EE->table->set_template(array(
			'table_open'    => '<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">',
			'row_start'     => '<tr class="even">'
		));
		$this->EE->table->set_heading(array('data' => lang('preference'), 'style' => 'width: 50%'), lang('setting'));
		$this->EE->table->add_row(
			lang('mrelshp_license_key', 'license_key'),
			form_input('license_key', $license_key, 'id="license_key" style="width:100%;"')
		);
		return $this->EE->table->generate();
	}

	/** -------------------------------------
	/** Save Global Settings
	/** -------------------------------------*/
	
	function save_global_settings()
	{
		return array(
			'license_key' => isset($_POST['license_key']) ? $_POST['license_key'] : ''
		);
	}
	
	// --------------------------------------------------------------------
	
	function display_field($data)
	{	
	
		$values_array = array();
	
		if ($this->settings['mrelshp_field_related_orderby'] == 'date')
		{
			$this->settings['mrelshp_field_related_orderby'] = 'entry_date';
		}
		
		$channels = decode_multi_field($this->settings['mrelshp_field_related_channel_id']);
				
		$this->EE->db->select('entry_id, title');		
		$this->EE->db->where_in('channel_id', $channels);
		$this->EE->db->order_by($this->settings['mrelshp_field_related_orderby'], $this->settings['mrelshp_field_related_sort']);
		
		if ($this->settings['mrelshp_field_related_max'] > 0)
		{
			$this->EE->db->limit($this->settings['mrelshp_field_related_max']);
		}
		
		$relquery = $this->EE->db->get('channel_titles');
		
		
		if ($relquery->num_rows() == 0)
		{
			return $this->EE->lang->line('no_related_entries');
		}
		else
		{
			if ( ! isset($_POST[$this->field_name])) // show form
			{
				if ($data){
					$data_array=decode_multi_field($data);
					$this->EE->db->select('rel_child_id');
					$this->EE->db->where_in('rel_id', $data_array);
					$query = $this->EE->db->get('relationships');
					foreach ($query->result_array() as $row)
						{
							$values_array[] = $row['rel_child_id'];
						}
				}
			}
			
			// single channel
			if ($this->settings['mrelshp_view_options']=='single')
			{
				foreach ($relquery->result_array() as $relrow)
				{
					$field_options[$relrow['entry_id']] = $relrow['title'];
				}
			}
			else // multi channels or optgroups as categories
			{
				$entries = '';
				foreach ($relquery->result_array() as $relrow)
				{
					$entries .=($entries==''?'':',').$relrow['entry_id'];
				}
				$channels = implode(',',$channels);
				
				$limit=($this->settings['mrelshp_field_related_max'] > 0)?'LIMIT '.$this->settings['mrelshp_field_related_max']:'';
				
				//multi
				if ($this->settings['mrelshp_view_options']=='multi'){
					// fetch our entries with channels titles
					$entry_optgroups_query = $this->EE->db->query("select 
					exp_channels.channel_title AS optgroups, 
					exp_channels.channel_id AS channel_id,
					exp_channel_titles.entry_id AS entry_id,
					exp_channel_titles.title AS entry_title
					FROM exp_channels
					INNER JOIN exp_channel_titles ON exp_channel_titles.channel_id = exp_channels.channel_id
					WHERE exp_channel_titles.entry_id IN (".$entries.")
					AND exp_channel_titles.channel_id IN (".$channels.")
					ORDER BY channel_title, ".$this->settings['mrelshp_field_related_orderby']." ".$this->settings['mrelshp_field_related_sort']." ".$limit." 
					");
				}
				
				// optgroups as categories
				if ($this->settings['mrelshp_view_options']=='optgroups')
				{
					// fetch our entries with cats
					$entry_optgroups_query = $this->EE->db->query("select 
					exp_categories.cat_name AS optgroups, 
					exp_categories.cat_id AS cat_id,
					exp_channel_titles.entry_id AS entry_id,
					exp_channel_titles.title AS entry_title
					FROM exp_categories
					INNER JOIN exp_category_posts ON exp_category_posts.cat_id = exp_categories.cat_id
					INNER JOIN exp_channel_titles ON exp_category_posts.entry_id = exp_channel_titles.entry_id
					WHERE exp_channel_titles.entry_id IN (".$entries.")
					AND exp_channel_titles.channel_id IN (".$channels.")
					ORDER BY cat_order, ".$this->settings['mrelshp_field_related_orderby']." ".$this->settings['mrelshp_field_related_sort']." ".$limit." 
					");
				}
				if($entry_optgroups_query->num_rows == 0)
				{
					return lang('no_entries_found');
				}
				// prepare array for optogroups
				$field_options = array();
				foreach ($entry_optgroups_query->result_array() as $option)
				{
					if(!isset($field_options[$option['optgroups']])) $field_options[$option['optgroups']] = array();
					$field_options[$option['optgroups']][$option['entry_id']]=$option['entry_title'];
				}
			}
			
			if($this->settings['mrelshp_extended_ui']== TRUE)
			{
				/*
				$this->EE->cp->add_js_script(array('ui' => 'core'));
				$this->EE->cp->add_js_script(array('ui' => 'draggable'));
				$this->EE->cp->add_js_script(array('ui' => 'droppable')); 
				$this->EE->cp->add_js_script(array('ui' => 'sortable')); 
				*/
				$this->EE->cp->add_to_foot('<script type="text/javascript" src="' .$this->_asset_path().'js/ui.multiselect.js"></script>');
				$this->EE->cp->add_to_head('<link type="text/css" href="' . $this->_asset_path().'css/ui.multiselect.css" rel="stylesheet" /> ');
			}
			$this->EE->javascript->output('
				var mrelshp_data_'.$this->field_id.' = $(\'#'.$this->field_id.'\');
				
				/*
				mrelshp_data_'.$this->field_id.'.change(function() {
					var v = mrelshp_data_'.$this->field_id.'.val();
					var jv = v?v.join(\'|\'):\'\';
					$(\'input[name="'.$this->field_name.'"]\').val(jv); 
				});
				*/
				if (typeof $.fn.multiselect != "undefined") mrelshp_data_'.$this->field_id.'.multiselect({sortable: false});
				
				$(\'#publishForm\').submit(function(){
					mrelshp_data_'.$this->field_id.'.each(function(){
						var o = $(this);
						var v = o.find(\'option:selected\');
						var jv = \'\';
						v.each(function(i){
							var slctd = $(this).val(); 
							jv += i==0?slctd:\'|\'+slctd;
						});
						$(\'input[name="'.$this->field_name.'"]\').val(jv);
					});
				});
				
			');
			$this->EE->javascript->compile(); 
			
			$values = encode_multi_field($values_array);
			$values = ($values=='')?'0':$values; //FF fix
			$__str = form_hidden($this->field_name,$values);
			
			$__str .= form_multiselect('fake_'.$this->field_name, $field_options, $values_array, 'id="'.$this->field_id.'" size="'.$this->settings['mrelshp_rows'].'" class="mrelshp_selectbox" style="width:92%;"');
			return $__str;
		}
	}
	function display_settings($data)
	{
		$this->EE->javascript->output('
				
				var mrelshp_field_related_max = $(\'#ft_mrelshp\').find(\'#field_related_max\').attr(\'name\',\'mrelshp_field_related_max\');
				var mrelshp_field_related_orderby = $(\'#ft_mrelshp\').find(\'#field_related_orderby\').attr(\'name\',\'mrelshp_field_related_orderby\');
				var mrelshp_field_related_sort = $(\'#ft_mrelshp\').find(\'#field_related_sort\').attr(\'name\',\'mrelshp_field_related_sort\');
				var select_field_related_channel_id = $(\'#ft_mrelshp\').find(\'select#field_related_channel_id\').attr(\'name\',\'mrelshp_field_related_channel_id\');
				$(\'#mrelshp_view_options\').change(function(){
					if($(this).val()==\'multi\')
					{
						select_field_related_channel_id.attr(\'multiple\',\'multiple\').attr(\'name\',\'mrelshp_field_related_channel_id_TMP\');
					 }else{
						if(select_field_related_channel_id.attr(\'multiple\')) {
							select_field_related_channel_id.removeAttr(\'multiple\').attr(\'name\',\'mrelshp_field_related_channel_id\');
						}
					}
				});
				
				$(\'#field_related_channel_id\').live(\'change\',function() {
					var v = $(this).val(); var jv = v?((typeof (v)==\'object\')?v.join(\'|\'):v):\'\';
					$(\'input[name="mrelshp_field_related_channel_id"]\').val(jv); 
				});
				
				var input_field_related_channel_id = $(\'input[name="mrelshp_field_related_channel_id"]\');
				
				if (input_field_related_channel_id.length && input_field_related_channel_id.val()!=\'\'){
					mrelshp_field_related_channel_ids=input_field_related_channel_id.val().split(\'|\');
					if (mrelshp_field_related_channel_ids.length>1 || $(\'#mrelshp_view_options\').val()==\'multi\'){
						select_field_related_channel_id.attr(\'multiple\',\'multiple\').attr(\'name\',\'field_related_channel_id_TMP\');					
						select_field_related_channel_id.find(\'option\').each(function(){
							var o = $(this); o.removeAttr("selected"); if ($.inArray(o.val(), mrelshp_field_related_channel_ids)>-1) o.attr("selected","selected");
						});
					}else{
						select_field_related_channel_id.find(\'option\').each(function(){
								var o = $(this); if ($.inArray(o.val(), mrelshp_field_related_channel_ids)>-1) o.attr("selected","selected");
							});
					}
				}
				
				mrelshp_field_related_orderby.find(\'option\').each(function(){
							var o = $(this);  if (o.val()=="'.((isset($data['mrelshp_field_related_orderby']))?$data['mrelshp_field_related_orderby']:'title').'") o.attr("selected","selected");
						});
				mrelshp_field_related_max.find(\'option\').each(function(){
							var o = $(this);  if (o.val()=="'.((isset($data['mrelshp_field_related_max']))?$data['mrelshp_field_related_max']:'0').'") o.attr("selected","selected");
						});
				mrelshp_field_related_sort.find(\'option\').each(function(){
							var o = $(this);  if (o.val()=="'.((isset($data['mrelshp_field_related_sort']))?$data['mrelshp_field_related_sort']:'desc').'") o.attr("selected","selected");
						});
			');
		
		$values = (isset($data['mrelshp_field_related_channel_id'])) ? $data['mrelshp_field_related_channel_id'] : '0';
		$values = ($values=='')?'0':$values; //FF fix
		$voption = (isset($data['mrelshp_view_options'])) ? $data['mrelshp_view_options'] : 'single';
		$this->EE->table->add_row(
			lang('mrelshp_type','mrelshp_view_options'), form_hidden('mrelshp_field_related_channel_id',$values).form_dropdown('mrelshp_view_options', $this->view_options, $voption,'id="mrelshp_view_options"')
		);
		
		parent::display_settings($data);
		
		$field_rows	= (!isset($data['mrelshp_rows']) || $data['mrelshp_rows'] == '') ? 6 : $data['mrelshp_rows'];
		$this->EE->table->add_row(
			lang('mrelshp_rows', 'mrelshp_rows'), form_input(array('id'=>'mrelshp_rows','name'=>'mrelshp_rows', 'size'=>4,'value'=>$field_rows))
		);
		
		$mrelshp_extended_ui = NULL;
			
		if(isset($data['mrelshp_extended_ui']))
		{
			$mrelshp_extended_ui = $data['mrelshp_extended_ui'];
		}

		$this->EE->table->add_row(
			lang('mrelshp_extended_ui','mrelshp_extended_ui'), form_checkbox('mrelshp_extended_ui', 1, $mrelshp_extended_ui)
		);
		
	}
	function save_settings($data)
	{		
	
		return array(
			'mrelshp_extended_ui' => $this->EE->input->post('mrelshp_extended_ui'),
			'mrelshp_view_options' => $this->EE->input->post('mrelshp_view_options'),
			'mrelshp_rows' => $this->EE->input->post('mrelshp_rows'),
			'mrelshp_field_related_channel_id' => $this->EE->input->post('mrelshp_field_related_channel_id'),
			
			'mrelshp_field_related_max' => $this->EE->input->post('mrelshp_field_related_max'),
			'mrelshp_field_related_orderby' => $this->EE->input->post('mrelshp_field_related_orderby'),
			'mrelshp_field_related_sort' => $this->EE->input->post('mrelshp_field_related_sort'),
		);
	}
	
	function settings_modify_column($data)
	{
		if ($data['ee_action'] == 'delete')
		{
			$this->EE->db->select('field_id_'.$data['field_id']);
			$this->EE->db->where('field_id_'.$data['field_id'].' !=', '0');
			$rquery = $this->EE->db->get('channel_data');

			if ($rquery->num_rows() > 0)
			{
				$rel_ids = array();

				foreach ($rquery->result_array() as $row)
				{
					$rel_ids[] = $row['field_id_'.$data['field_id']];
				}

				$this->EE->db->where_in('rel_id', $rel_ids);
				$this->EE->db->delete('relationships');
			}
		}
	
		$fields['field_id_'.$data['field_id']] = array(
			'type' 			=> 'TEXT'
			);	

		return $fields;
	}	
}

// END Mrelshp_ft class

/* End of file ft.mrelshp.php */
/* Location: ./system/expressionengine/fieldtypes/ft.mrelshp.php */