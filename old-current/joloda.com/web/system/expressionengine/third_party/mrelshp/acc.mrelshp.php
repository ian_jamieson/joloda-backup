<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
class Mrelshp_acc {

	var $name		= 'Mrelshp FT helper';
	var $id			= 'Mrelshp';
	var $version		= '1.5';
	var $description	= 'Multi Relationships jQuery Dropdown Check List';
	var $sections		= array();
	
	
	/**
	 * Constructor
	 */
	function Mrelshp_acc()
	{
		$this->EE =& get_instance();
	}
	
	
	/** -------------------------------------
	/** Theme URL
	/** -------------------------------------*/
	function _asset_path()
	{
		if (! isset($this->cache['theme_url']))
		{
			$theme_folder_url = $this->EE->config->item('theme_folder_url');
			if (substr($theme_folder_url, -1) != '/') $theme_folder_url .= '/';
			$this->cache['theme_url'] = $theme_folder_url.'third_party/mrelshp/assets/';
		}
		return $this->cache['theme_url'];
	}
	
	
	
	/**
	 * Set Sections
	 *
	 * Set content for the accessory
	 *
	 * @access	public
	 * @return	void
	 */
	
	function set_sections()
	{
		$this->EE->load->library('javascript');
		$this->EE->javascript->output('$("#Mrelshp.accessory").remove();$("#accessoryTabs").find("a.Mrelshp").parent("li").remove();');
					
		if ($this->EE->input->get_post('C') == 'content_publish' && $this->EE->input->get_post('M') == 'entry_form')
        {
			$this->EE->cp->add_to_head('<link type="text/css" href="'  . $this->_asset_path() . 'css/ui.dropdownchecklist.css" rel="stylesheet" /> ');
	
			$this->EE->cp->add_to_foot('
			<script type="text/javascript" src="' . $this->_asset_path() . 'js/ui.dropdownchecklist.js"></script>
			<script type="text/javascript">
				if (typeof $.fn.multiselect == "undefined") {
					$(".mrelshp_selectbox").dropdownchecklist();
					$(".ui-dropdownchecklist-item input").removeAttr("disabled");
					$(".ui-dropdownchecklist").unbind("click");
					$(document).unbind("click");
				}
			</script>');
		}
		
		
		if($this->EE->input->get('D') == 'cp' AND $this->EE->input->get('C') == 'addons_accessories')
        {
           $this->EE->db->where('class', 'Mrelshp_acc');
           $this->EE->db->update('accessories', array('controllers' => 'content_publish'));
        }
		
	}
}
// END CLASS