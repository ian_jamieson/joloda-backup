<?php

if ( ! defined('CE_CACHE_VERSION') )
{
	//Last Update: 12 April 2013
	define('CE_CACHE_VERSION', '1.9.4');
}

$config['name'] = 'CE Cache';
$config['version'] = CE_CACHE_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://www.causingeffect.com/software/expressionengine/ce-cache/change-log.rss';