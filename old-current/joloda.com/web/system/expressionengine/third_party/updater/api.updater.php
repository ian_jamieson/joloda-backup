<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (defined('PATH_THIRD') && file_exists(PATH_THIRD.'updater/config.php') === TRUE) include_once PATH_THIRD.'updater/config.php';
else include_once dirname(dirname(__FILE__)).'/updater/config.php';

/**
 * Updater API File
 *
 * @package         DevDemon_Updater
 * @author          DevDemon <http://www.devdemon.com> - Lead Developer @ Parscale Media
 * @copyright       Copyright (c) Parscale Media <http://www.parscale.com>
 * @license         http://www.devdemon.com/license/
 * @link            http://www.devdemon.com/updater/
 */
class Updater_API
{
    private $EE;

    public $settings = array();

    // ********************************************************************************* //

    public function __construct()
    {
        $this->EE =& get_instance();

        $this->stats = new stdClass();
        $this->stats->event = '';
        $this->stats->data = array();
    }

    // ********************************************************************************* //

    public function sendStats()
    {
        if (!function_exists('curl_init')) return;

        // Are we allowed to track stats?
        if (isset($this->settings['track_stats']) === true && $this->settings['track_stats'] == 'no') {
            return;
        }

        try {
            $host = 'http://api.mixpanel.com/track/?data=';
            $token = '8258f45c0b24fafe438deaec812be1a0';

            $params = array();
            $params['event'] = $this->stats->event;
            $params['properties'] = $this->stats->data;

            if (!isset($params['properties']['token'])){
                $params['properties']['token'] = $token;
            }

            // During EE Update, this is not available
            if (defined('APP_BUILD'))
            {
                $params['properties']['app_build'] = APP_BUILD;
                $params['properties']['app_version'] = APP_VER;
            }

            $params['properties']['server_os'] = (DIRECTORY_SEPARATOR == '/') ? 'unix' : 'windows';
            $params['properties']['updater_version'] = UPDATER_VERSION;
            $params['properties']['transfer_method'] = $this->settings['file_transfer_method'];

            $php_version = explode('.', PHP_VERSION);
            $params['properties']['php_version'] = @$php_version[0].'.'.@$php_version[1].'.'.@$php_version[2];

            if (isset($this->settings['mixpanel_token']) === true)
            {
                $params['properties']['token'] = $this->settings['mixpanel_token'];
            }

            //$this->EE->firephp->log($params);

            $data = base64_encode($this->EE->updater_helper->generate_json($params));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host.$data);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $resp = curl_exec($ch);
            curl_close($ch);

            $this->stats->event = '';
            $this->stats->data = array();

        } catch (Exception $e) {

        }
    }

    // ********************************************************************************* //

} // END CLASS

/* End of file api.updater.php  */
/* Location: ./system/expressionengine/third_party/updater/api.updater.php */
