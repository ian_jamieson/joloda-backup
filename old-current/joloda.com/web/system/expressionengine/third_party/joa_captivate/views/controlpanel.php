<?php

?>
<?= form_open(
			'C=addons_extensions&M=extension_settings&file=' . $addon_id,
			array('id' => $addon_id . '_prefs'),
			array($input_prefix."[enabled]" => TRUE)
		)
?>

	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th style="width:50%;"><?php echo $lang->line('preference'); ?></th>
				<th><?php echo $lang->line('setting'); ?></th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td><strong><?php echo $lang->line('switched_on'); ?></strong></td>
				<td>
					<input <?php echo ($settings['switched_on'] == 'yes' ? 'checked="checked"' : ""); ?>type="radio" name="switched_on" value="yes" id="switched_on" label="yes">&nbsp;<label for="switched_on"><?php echo $lang->line('yes'); ?></label>
					<input <?php echo ($settings['switched_on'] == 'no' ? 'checked="checked"' : "");  ?>type="radio" name="switched_on" value="no" id="switched_off" label="no">&nbsp;<label for="switched_off"><?php echo $lang->line('no'); ?></label>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('nucaptcha_on'); ?></strong></td>
				<td>
					<input <?php echo ($settings['nucaptcha_on'] == 'yes' ? 'checked="checked"' : ""); ?>type="radio" name="nucaptcha_on" value="yes" id="nucaptcha_on" label="yes">&nbsp;<label for="nucaptcha_on"><?php echo $lang->line('yes'); ?></label>
					<input <?php echo ($settings['nucaptcha_on'] == 'no' ? 'checked="checked"' : "");  ?>type="radio" name="nucaptcha_on" value="no" id="nucaptcha_off" label="no">&nbsp;<label for="nucaptcha_off"><?php echo $lang->line('no'); ?></label>
				</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('nucaptcha_key'); ?></strong></td>
				<td>
					<input type="text" name="nucaptcha_key" value="<?php echo $settings['nucaptcha_key']; ?>" id="nucaptcha_key" /> 
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('recaptcha_on'); ?></strong></td>
				<td>
					<input <?php echo ($settings['recaptcha_on'] == 'yes' ? 'checked="checked"' : ""); ?>type="radio" name="recaptcha_on" value="yes" id="recaptcha_on" label="yes">&nbsp;<label for="recaptcha_on"><?php echo $lang->line('yes'); ?></label>
					<input <?php echo ($settings['recaptcha_on'] == 'no' ? 'checked="checked"' : "");  ?>type="radio" name="recaptcha_on" value="no" id="recaptcha_off" label="no">&nbsp;<label for="recaptcha_off"><?php echo $lang->line('no'); ?></label>
				</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('recaptcha_public_key'); ?></strong></td>
				<td>
					<input type="text" name="recaptcha_public_key" value="<?php echo $settings['recaptcha_public_key']; ?>" id="recaptcha_public_key" /> 
				</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('recaptcha_private_key'); ?></strong></td>
				<td>
					<input type="text" name="recaptcha_private_key" value="<?php echo $settings['recaptcha_private_key']; ?>" id="recaptcha_private_key" /> 
				</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('recaptcha_language'); ?></strong></td>
				<td>
					<input type="text" name="recaptcha_language" value="<?php echo $settings['recaptcha_language']; ?>" id="recaptcha_language" /> 
				</td>
			</tr>
			<tr class="even">
				<td><strong><?php echo $lang->line('recaptcha_theme'); ?></strong></td>
				<td>
					<select name="recaptcha_theme" id="recaptcha_theme" />
						<option value="red"<?php echo ($settings['recaptcha_theme'] == "red" ? " SELECTED" : ""); ?>>Red</option>
						<option value="white"<?php echo ($settings['recaptcha_theme'] == "white" ? " SELECTED" : ""); ?>>White</option>
						<option value="blackglass"<?php echo ($settings['recaptcha_theme'] == "blackglass" ? " SELECTED" : ""); ?>>Blackglass</option>
						<option value="clean"<?php echo ($settings['recaptcha_theme'] == "clean" ? " SELECTED" : ""); ?>>Clean</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>

	<input type="submit" name="submit" value="Submit" class="submit">
</form>
<div class="clear_right"></div>
