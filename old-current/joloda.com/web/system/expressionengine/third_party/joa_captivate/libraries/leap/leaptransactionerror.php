<?php
/**
 * @package   NuCaptcha PHP clientlib
 * @author    <support@nucaptcha.com> Leap Marketing Technologies Inc
 * @license   LGPL License 2.1 (see included license.txt)
 * @link      http://www.nucaptcha.com/api/php
 */

class lmTransactionError extends lmTransactionInterface
{
    private $mFailureHTML = "";
    private $mPersistentData = false;

	/**
	 * Constructor:
	 * Stores the input for later on
	 *
	 * @param string $failureHTML		- the html to return in GetWidget()
	 * @param string $persistentData	- the data to return in GetPersistentData()
	 */
    public function __construct($failureHTML, $persistentData)
    {
        $this->mFailureHTML = $failureHTML;
        $this->mPersistentData = $persistentData;
    }

	/**
	 * Initialize:
	 * Doesn't do anything for this particular class
	 */
	public function Initialize(lmTextChunk $treq, $tokenkey)
	{
	}

	/**
	 * Not doing anything with sockets, so just make it an empty function
	 */
	protected function CheckSocketRead()
	{
	}

	/**
	 * GetPersistentData:
	 * Returns the error persistent data
	 *
	 * @return string
	 */
	public function GetPersistentData()
    {
        return $this->mPersistentData;
    }

	/**
	 * GetLinks:
	 *
	 * @return string
	 */
	public function GetLinks()
	{
		return "";
	}

	/**
	 * GetHTML:
	 * Returns the error HTML
	 *
	 * @return string
	 */
	public function GetHTML($position = "left")
	{
		return $this->mFailureHTML;
	}

	public function GetJavascript(
		$isFlashTransparent = false,
		$setFocusToAnswerBox = false,
		$position = Leap::POSITION_LEFT,
		$preferredLanguage = lmTransactionInterface::LANGUAGE_ENGLISH
	)
	{
		return "";
	}

	public function GetJavascriptToReinitialize(
			$isFlashTransparent = false,
			$setFocusToAnswerBox = false,
			$position = Leap::POSITION_LEFT,
			$preferredLanguage = lmTransactionInterface::LANGUAGE_ENGLISH
	)
	{
		return "";
	}

	public function GetJSONToReinitialize(
			$extraParameters = null,
			$isFlashTransparent = false,
			$setFocusToAnswerBox = false,
			$position = Leap::POSITION_LEFT,
			$preferredLanguage =
			lmTransactionInterface::LANGUAGE_ENGLISH
	)
	{
		return "";
	}

	public function GetWidget(
			$isFlashTransparent = false,
			$setFocusToAnswerBox = false,
			$position = Leap::POSITION_LEFT,
			$preferredLanguage = lmTransactionInterface::LANGUAGE_ENGLISH
	)
	{
		return "";
	}

	public function GetJavascriptReinitializeFunctionName()
	{
		return "";
	}

	protected function decodeTRESChunk(lmTextChunkData $TRES)
	{

	}
}
