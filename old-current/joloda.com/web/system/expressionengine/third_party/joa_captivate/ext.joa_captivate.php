<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * View for Control Panel Settings Form
 * This file is responsible for displaying the user-configurable settings for the JOA Captivate extension in the ExpressionEngine control panel.
 *
 * @package			JoaCaptivate
 * @version			0.1
 * @author			Joachim Rijsdam <http://bytedesign.nl> - Technical Director, ByteDESiGN
 * @copyright 		Copyright (c) 2010 ByteDESiGN <http://bytedesign.nl>
 * @license 		Public - please see LICENSE file included with this distribution
 * @link				http://expressionengine-addons.com/joa-another-captcha
 * @see 				http://expressionengine.com/public_beta/docs/development/extensions.html
 **/

/**
 * Version 0.5 20101004
 * --------------------
 * cleaned up the language file
 *
 *
 * Version 0.5 20101004
 * --------------------
 * Initial public release
 */
class Joa_captivate_ext {

	var $name            = 'JOA Captivate';
	var $version         = '0.5';
	var $description     = 'Convert the default graphic captcha into something else';
	var $settings_exist  = 'y';
	var $docs_url        = 'http://bytedesign.nl/blog/captivate';
	var $settings        = array();
	
	/**
	 * Constructor
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function Joa_captivate_ext($settings = '')
	{

		$this->addon_id = strtolower(substr(__CLASS__, 0, -4));
    	$this->settings = $settings;
	}
	
	/**
	 * Activate Extension
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function activate_extension()
	{
		$EE =& get_instance();

		$data = array();

		// create captcha
		$data['class']				= __CLASS__;
		$data['method']			= "create_captcha";
		$data['hook']				= "create_captcha_start";
		$data['settings']			= "";
		$data['priority']			= 10;
		$data['version']			= $this->version;
		$data['enabled']			= "y";
		$EE->db->insert('extensions', $data);

		// validate captcha
		$data['class']			= __CLASS__;
		$data['method']		= "validate_captcha";
		$data['hook']			= "member_member_register_start";
		$data['settings']		= "";
		$data['priority']		= 10;
		$data['version']		= $this->version;
		$data['enabled']		= "y";
		$EE->db->insert('extensions', $data);

		// validate captcha
		$data['class']			= __CLASS__;
		$data['method']		= "validate_captcha";
		$data['hook']			= "insert_comment_start";
		$data['settings']		= "";
		$data['priority']		= 10;
		$data['version']		= $this->version;
		$data['enabled']		= "y";
		$EE->db->insert('extensions', $data);

		// FREEFORM validate captcha
		$data['class']			= __CLASS__;
		$data['method']		= "validate_captcha";
		$data['hook']			= "freeform_module_validate_end";
		$data['settings']		= "";
		$data['priority']		= 10;
		$data['version']		= $this->version;
		$data['enabled']		= "y";
		$EE->db->insert('extensions', $data);

		// modify language
		$data['class']			= __CLASS__;
		$data['method']		= "lang_override";
		$data['hook']			= "sessions_end";
		$data['settings']		= "";
		$data['priority']		= 10;
		$data['version']		= $this->version;
		$data['enabled']		= "y";
		$EE->db->insert('extensions', $data);

		$dbtable = $EE->db->dbprefix . 'captivate';

		$query = "CREATE TABLE IF NOT EXISTS `".$dbtable."` (
			`captcha_id` bigint(13) unsigned,
			`date` int(10) unsigned NOT NULL,
			`ip_address` varchar(16) NOT NULL DEFAULT '0',
			`payload` TEXT NOT NULL,
			PRIMARY KEY (`captcha_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8";
		$EE->db->query($query);

	}

	/**
	 * Update Extension
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function update_extension($current = '')
	{
		$EE =& get_instance();

		$status = TRUE;
		
		if ($this->version != $current)
		{
			$data = array();
			$data['version'] = $this->version;
			$EE->db->update('extensions', $data, 'version = '.$current);
			
			if($EE->db->affected_rows() != 1)
			{
				$status = FALSE;
			}
		}
		
		return $status;
	}

	/**
	 * Disable Extensions
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function disable_extension()
	{
		$EE =& get_instance();
		$EE->db->where('class', __CLASS__);
    	$EE->db->delete('extensions');

		$dbtable = $EE->db->dbprefix . 'captivate';
		$query = "DROP TABLE IF EXISTS ".$dbtable;
		$EE->db->query($query);
	}
	
	/**
	 * Settings Form
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function settings_form($current)
	{
		$EE =& get_instance();
		$EE->lang->loadfile($this->addon_id);

		if($data = $EE->input->post(__CLASS__))
		{
			$this->_save_settings();
			return;
		}

		// Create the variable array
		$vars = array(
			'addon_id' => $this->addon_id,
			'error' => FALSE,
			'input_prefix' => __CLASS__,
			'message' => FALSE,
		);

		$EE->cp->load_package_js('joa_captivate');
		$EE->cp->load_package_css('joa_captivate');
		$EE->javascript->compile();

		$site_id = $EE->config->item('site_id');

		$settings_template = array(
			'switched_on' => 'no',
			'nucaptcha_on' => 'no',
			'nucaptcha_key' => '',
			'recaptcha_on' => 'no',
			'recaptcha_public_key' => '',
			'recaptcha_private_key' => '',
			'recaptcha_language' => '',
			'recaptcha_theme' => '',
		);

		$vars['settings'] = (isset($current[$site_id])) ? $current[$site_id] : $settings_template;
		$vars['lang'] = $EE->lang;
		$vars['BASE'] = str_replace('&amp;', '&', BASE);

		$EE->load->helper('form');
		return $EE->load->view('controlpanel', $vars, TRUE);
	}
	
	/**
	 * Save Settings
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Private
	 */
	function _save_settings()
	{
		$EE =& get_instance();

		if (empty($_POST))
		{
			show_error($EE->lang->line('unauthorized_access'));
			return;
		}
		
		unset($_POST['submit']);
		$EE->lang->loadfile($this->addon_id);
		$EE->load->helper('array');

		$switched_on 		= $EE->input->post('switched_on');

		$nucaptcha_on 		= $EE->input->post('nucaptcha_on');
		$nucaptcha_key 	= $EE->input->post('nucaptcha_key');

		$recaptcha_on 				= $EE->input->post('recaptcha_on');
		$recaptcha_private_key 	= $EE->input->post('recaptcha_private_key');
		$recaptcha_public_key 	= $EE->input->post('recaptcha_public_key');
		$recaptcha_language	 	= $EE->input->post('recaptcha_language');
		$recaptcha_theme		 	= $EE->input->post('recaptcha_theme');
		
		$this->_valid_or_redirect($switched_on, array('yes', 'no'));
		$this->_valid_or_redirect($nucaptcha_on, array('yes', 'no'));
		$this->_valid_or_redirect($recaptcha_on, array('yes', 'no'));
				
		$site_id = $EE->config->item('site_id');
		$EE->db->where('class', __CLASS__);
		$query = $EE->db->get('extensions', 1, 0);

		$save_data = array();
		
		if ($query->num_rows() == 1)
		{
			$data = $query->row();
			$save_data = unserialize($data->settings);
		}

		$save_data[$site_id]['switched_on'] = $switched_on;

		$save_data[$site_id]['nucaptcha_on'] = $nucaptcha_on;
		$save_data[$site_id]['nucaptcha_key'] = $nucaptcha_key;

		$save_data[$site_id]['recaptcha_on'] = $recaptcha_on;
		$save_data[$site_id]['recaptcha_private_key'] = $recaptcha_private_key;
		$save_data[$site_id]['recaptcha_public_key'] = $recaptcha_public_key;
		$save_data[$site_id]['recaptcha_language'] = $recaptcha_language;
		$save_data[$site_id]['recaptcha_theme'] = $recaptcha_theme;
	
		$EE->db->where('class', __CLASS__);
		$EE->db->update('extensions', 
			array('settings' => serialize($save_data))
		);
		
		$EE->session->set_flashdata('message_success', $EE->lang->line('preferences_updated'));
		$EE->functions->redirect(
			BASE.AMP.'C=addons_extensions'.AMP.'M=extension_settings'.AMP.'file=joa_captivate'
		);
	}

	/**
	 * Create Captcha Hook
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 * @param		<string> Unused by this extension
	 */
	function create_captcha($old_word = '')
	{

		// bail out if the prereqs aren't met
		if ($this->check_settings() !== TRUE)
		{
			return;
		}

		// acquire settings
		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		$settings = $this->settings[$site_id];

		// determine the set of available captcha's
		$selection = array();
		if( $settings['nucaptcha_on'] == 'yes' ) $selection[] = 'nucaptcha';
		if( $settings['recaptcha_on'] == 'yes' ) $selection[] = 'recaptcha';

		// if everything is disabled, bail out and hope for the best
		if(sizeof($selection) == 0)
		{
			return;
		}

		// we don't use the old_word, so unset it here
		unset($old_word);

		// choose from the available captcha methods
		$choice = mt_rand(0, sizeof($selection)-1);
		switch( $selection[$choice] ) {
		case 'nucaptcha':
			return $this->_create_nucaptcha();
		case 'recaptcha':
			return $this->_create_recaptcha();
		default:
			// we shouldn't be able to get to this choice, maybe change it to a default choice ?
			return;
		}
	}


	/**
	 * Create NuCaptcha
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Private
	 */
	function _create_nucaptcha()
	{
		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		$settings = $this->settings[$site_id];

		require_once("libraries/leap/leapmarketingclient.php");

		// Your ClientKey is supplied by Leap and can be downloaded from the publisher dashboard
		Leap::SetClientKey( $settings['nucaptcha_key']);

		$t = Leap::InitializeTransaction();

		// check if the transaction initialization was successful
		if(LMEC_OK == Leap::GetErrorCode())
		{

			$vars = array();
			$vars['date'] = time();
			$vars['ip_address'] = $EE->input->ip_address();
			$vars['word'] = 'NuCaptcha';
			$EE->db->insert('captcha', $vars, TRUE);
			$captcha_id = $EE->db->insert_id();

			$vars2 = array();
			$vars2['captcha_id'] = 	$captcha_id;			// get these three values from the previous insertion, from the captcha table
			$vars2['ip_address'] = 	$vars['ip_address'];
			$vars2['date'] = 			$vars['date'];
			$vars2['payload'] = 				json_encode($t->GetPersistentData());
			$EE->db->insert('captivate', $vars2, TRUE);

			 // and get the actual player code
			 $output = $t->GetWidget();
		}
		else
		{
			// TODO put a proper message here, like 'refresh the page in a couple of seconds'

			 // report a problem to the user if transaction initialization failed for some reason
			 $output = "NuCaptcha failed to load; please refresh this page.<br />Error message was: " . Leap::GetErrorString();
		}

		$EE->extensions->end_script = TRUE;
		return $output;
	}


	/**
	 * Create ReCaptcha
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Private
	 */
	function _create_recaptcha()
	{
		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		$settings = $this->settings[$site_id];

		// Load the stock recaptcha PHP library (v1.11 as of this writing)
		require_once('libraries/recaptchalib.php');

		$vars['date'] = time();
		$vars['ip_address'] = $EE->input->ip_address();
		$vars['word'] = 'ReCaptcha';
		$EE->db->insert('captcha', $vars);

		$theme = $settings['recaptcha_theme'];
		$language = $settings['recaptcha_language'];


		// EE's validation error page uses javascript for the "Return to Previous Page" link.
		// Different browsers cache form elements differently, which can cause the validation
		// request to be posted with an old challenge key and fail. Hence, recaptchaFix().

		$output = <<<EOD
			<script type="text/javascript">
				var RecaptchaOptions = {theme   : '$theme',
										callback: recaptchaFix};
				
				function recaptchaFix()
				{
					var container = document.getElementById('recaptcha_challenge_field_holder');
					container.innerHTML = '<input type="hidden" value="' + Recaptcha.get_challenge() +'" id="recaptcha_challenge_field" name="recaptcha_challenge_field">';
				}
			</script>
EOD;

		$output .= recaptcha_get_html(trim($settings['recaptcha_public_key']));
		$EE->extensions->end_script = TRUE;

		return $output;
	}

	function check_settings() {

		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		
		// If settings are empty or wrong, try to fall back to the regular
		// captcha and hope they haven't removed its input field yet		

		if ( ! isset($this->settings[$site_id]))
		{
			$EE->extensions->end_script = FALSE;
			return FALSE;
		}
		
		$settings = $this->settings[$site_id];
		if ($settings['switched_on'] == 'no')
		{
			$EE->extensions->end_script = FALSE;
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Validate Captcha Hook
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function validate_captcha()
	{
		$EE =& get_instance();

		// get the NuCaptcha payload from the database
		$EE->db->where('ip_address', $EE->input->ip_address());
		$EE->db->order_by('date', 'DESC');
		$EE->db->limit(1);
		$query = $EE->db->get('captcha'); 

		// bail out if no entries found
		if( $query->num_rows() == 0 ) {
			$_POST['captcha'] = '';
			return;
		}

		$arr = $query->row_array();
		switch( $arr['word'] ) {
			case 'NuCaptcha':
				return $this->_validate_nucaptcha();
			case 'ReCaptcha':
				return $this->_validate_recaptcha();
			default:
				return;
		}
	}

	function _validate_nucaptcha()
	{

		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		$settings = $this->settings[$site_id];

		require_once("libraries/leap/leapmarketingclient.php");

		// Your ClientKey is supplied by Leap and can be downloaded from the publisher dashboard
		Leap::SetClientKey($settings['nucaptcha_key']);

		// get the NuCaptcha payload from the database
		$EE->db->where('ip_address', $EE->input->ip_address());
		$EE->db->order_by('date', 'DESC');
		$EE->db->limit(1);
		$query = $EE->db->get('captivate'); 

		// bail out if no entries found
		if( $query->num_rows() == 0 ) {
			$_POST['captcha'] = '';
			return;
		}

		$arr = $query->row_array();
		$payload = json_decode($arr['payload']);

		// Check if the persistent data was stored, and if the user actually submitted an answer
		if(true === Leap::WasSubmitted())
		{
			// validate the transaction
			$valid = Leap::ValidateTransaction($payload);

			// check the result
			if (true === $valid)
			{
				// We have a valid captcha
				// Give EE what it's looking for
				$_POST['captcha'] = 'NuCaptcha';
			}
			else
			{
				$_POST['captcha'] = '';
				
				// validation failed; find out why.
// FIXME: this code is not really necessary.
//				switch (Leap::GetErrorCode())
//				{
//				case LMEC_WRONG:
//				case LMEC_EMPTY:
//					$transaction_status = "Answer was incorrect.";
//				break;
//
//				default:
//					$transaction_status = sprintf('Error Code: %s Error Message: %s',
//						Leap::GetErrorCode(),
//						Leap::GetErrorString()
//					);
//				break;
//				}
			}
		}

	}

	function _validate_recaptcha()
	{
		// If settings are obviously wrong, try to fall back to the regular
		// captcha and hope they haven't already removed its input field

		if ($this->check_settings() !== TRUE)
		{
			return;
		}

		$EE =& get_instance();
		$site_id = $EE->config->item('site_id');
		$settings = $this->settings[$site_id];

		$EE->lang->loadfile($this->addon_id);

		// Load the stock recaptcha PHP library (v1.11 as of this writing)
		require_once('libraries/recaptchalib.php');

		$private_key = trim($settings['recaptcha_private_key']);

		$response = recaptcha_check_answer($private_key,
		   $EE->input->ip_address(),
		   $EE->input->post('recaptcha_challenge_field'),
		   $EE->input->post('recaptcha_response_field')
		  );
		
		if ($response->is_valid === TRUE)
		{
			// Give EE what it's looking for
			$_POST['captcha'] = 'ReCaptcha';
		}
		else
		{
			// Ensure EE knows the captcha was invalid
			$_POST['captcha'] = '';

			// Whether the user's response was empty or just wrong, all we can do is make EE
			// think the captcha is missing, so we'll use more generic language for an error. 

			$EE->lang->language['captcha_required'] = $EE->lang->language['captivate_error'];
		}

		return;
	}
	
	/**
	 * Lang Override
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Public
	 */
	function lang_override()
	{
		$EE =& get_instance();

		$site_id = $EE->config->item('site_id');
		
		if ( ! isset($this->settings[$site_id]))
		{
			$EE->extensions->end_script = FALSE;
			return;
		}
		
		$EE->lang->loadfile('joa_captivate');
		$captcha_required = $EE->lang->line('captcha_required');
		$captcha_incorrect = $EE->lang->line('captcha_incorrect');
		$EE->lang->loadfile('core');
		
		// Override the lang.core.php keys for captchas
		$EE->lang->language['captcha_required'] = $captcha_required;
		$EE->lang->language['captcha_incorrect'] = $captcha_incorrect;
	}
	
	/**
	 * Validate function arguments or redirect
	 *
	 * @author		Joachim Rijsdam <info@bytedesign.nl>
	 * @copyright	Copyright (c) 2010 ByteDESiGN
	 * @access		Private
	 */
	function _valid_or_redirect()
	{
		$EE =& get_instance();

		$var = func_get_arg(0);
		$tests = func_get_arg(1);
		if (! in_array($var, $tests))
		{
			$EE->session->set_flashdata('message_failure', $EE->lang->line('save_failure'));
			$EE->functions->redirect(
				BASE.AMP.'C=addons_extensions'.AMP.'M=extension_settings'.AMP.'file=joa_captivate'
			);
		}
	}
}

/* End of file ext.joa_captivate.php */
/* Location: ./system/expressionengine/third_party/joa_captivate/ext.joa_captivate.php */
