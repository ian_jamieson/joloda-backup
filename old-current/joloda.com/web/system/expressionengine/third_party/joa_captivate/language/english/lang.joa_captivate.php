<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(
	
	'switched_on' =>
		'Do you want to enable Captivate for this site?',

	'nucaptcha_on' =>
		'Do you want to enable NuCaptcha for this site?',

	'nucaptcha_key' =>
		'NuCaptcha key',

	'recaptcha_on' =>
		'Do you want to enable ReCaptcha for this site?',

	'recaptcha_public_key' =>
		'ReCaptcha public key',

	'recaptcha_private_key' =>
		'ReCaptcha private key',

	'recaptcha_language' =>
		'ReCaptcha language',

	'recaptcha_theme' =>
		'ReCaptcha theme',

	'preference' =>
		'Preference',
	
	'setting' =>
		'Setting',

	'another_captcha_error' =>
		'default captcha error message here',

);


