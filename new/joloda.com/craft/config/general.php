<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

'usePathInfo' => true,
'omitScriptNameInUrls' => true,
'siteUrl' => array(
        'en' => 'http://beta.joloda.com/',
        'es' => 'http://beta.joloda.com/es/',
        'de' => 'http://beta.joloda.com/de/',
    ),

);
